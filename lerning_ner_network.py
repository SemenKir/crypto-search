import io
import json
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer,tokenizer_from_json
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Embedding
from sklearn.model_selection import train_test_split
import nltk
from nltk.stem.snowball import SnowballStemmer
import pandas as pd
import numpy as np
import re
from collections import Counter
nltk.download('averaged_perceptron_tagger_ru')
nltk.download('stopwords')
max_comment_length = 300
embedding_dim = 16
sequence_length = 300
PATH_TO_TOKENIZER = 'tokenizer_crypto.json'

def upperCaseRate(string):
    """
    Функция возвращения процента прописных букв в строке
    :param string: строка
    :return: возвращает процент прописных букв в строке
    """
    return np.array(list(map(str.isupper, string))).mean()
def cleanText(string):
    """
    Эта функция удаляет все символы, кроме кириллицы, латинского алфавита,
    стоп - слов, функциональных частей речи. Возвращает строку слов stem.
    :param string: строка текста
    :return: очищенная строка
    """
    # Общая чистка
    string = string.lower()
    string = re.sub(r"http\S+", "", string)
    string = str.replace(string, 'Ё', 'е')
    string = str.replace(string, 'ё', 'е')
    prog = re.compile('[А-Яа-яA-Za-z]+')
    words = prog.findall(string.lower())
    # Очищение слов
    ## Стоп слова
    stopwords = nltk.corpus.stopwords.words('russian')
    words = [w for w in words if w not in stopwords]
    ## Cleaning functional POS (Parts of Speech)
    functionalPos = {'CONJ', 'PRCL'}
    words = [w for w, pos in nltk.pos_tag(words, lang='rus') if pos not in functionalPos]
    ## Stemming
    stemmer = SnowballStemmer('russian')
    return ' '.join(list(map(stemmer.stem, words)))

def counter_word(t):
    count = Counter()
    for i in t:
        for word in i.split():
            count[word] += 1
    return count
def train_neural_network():
    """
    Обучение нейросети
    """
    X_tested, X_trained, max_features, y_test, y_train,_ = prepare_train_predict()
    model = Sequential()
    model.add(Embedding(max_features, embedding_dim, input_length=sequence_length,
                        embeddings_regularizer=l2(0.005)))  ####обьявление dim
    model.add(Dropout(0.4))
    model.add(LSTM(embedding_dim, dropout=0.2, recurrent_dropout=0.2,
                   return_sequences=True, kernel_regularizer=l2(0.005),
                   bias_regularizer=l2(0.005)))
    model.add(Flatten())
    model.add(Dense(512, activation='relu',
                    kernel_regularizer=l2(0.001),
                    bias_regularizer=l2(0.001), ))
    model.add(Dropout(0.4))
    model.add(Dense(8, activation='relu',
                    kernel_regularizer=l2(0.001),
                    bias_regularizer=l2(0.001), ))
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    model.compile(loss='binary_crossentropy', optimizer=Adam(1e-3), metrics=['accuracy'])
    epochs = 10
    model.fit(X_trained, y_train, epochs=epochs, validation_data=(X_tested, y_test), batch_size=2048)
    # сохранение модели
    model.save("model_crypto.h5")
    # проверка на тестовых данных
    predictions = model.predict(X_tested, verbose=1)
    print(predictions[:10])
    print(f'Total accuracy  {np.sum((np.round(predictions).flatten())) / sum(y_test) * 100}')

def prepare_train_predict():
    """
    Подготовка данных для обучения нейросети
    """
    data = pd.read_csv('train_shuffle.csv')
    text = np.array(data.mess.values)
    target = data.val.astype(int).values
    text = list(map(cleanText, text))
    X_train, X_test, y_train, y_test = train_test_split(text, target, test_size=0.2, stratify=target,
                                                        shuffle=True, random_state=42)
    dictionary_size = len(counter_word(text))
    tokenizer = Tokenizer(num_words=dictionary_size)
    tokenizer.fit_on_texts(X_train)
    X_train_tokenized_lst = tokenizer.texts_to_sequences(X_train)
    X_test_tokenized_lst = tokenizer.texts_to_sequences(X_test)
    X_trained = pad_sequences(X_train_tokenized_lst, maxlen=max_comment_length)
    X_tested = pad_sequences(X_test_tokenized_lst, maxlen=max_comment_length)
    max_features = len(counter_word(text))
    tokenizer_json = tokenizer.to_json()
    with io.open(PATH_TO_TOKENIZER, 'w', encoding='utf-8') as f:
        f.write(json.dumps(tokenizer_json, ensure_ascii=False))
    return X_tested, X_trained, max_features, y_test, y_train,tokenizer

def test_my_data(data_comment: list, model_neiro):
    """
    Функция тестирования на собственных данных
    :param data_comment: массив строк, содержищих высказывания
    :param model_neiro: модель нейросети
    :return: массив пердсказания
    """
    with open(PATH_TO_TOKENIZER) as f:
        data = json.load(f)
        tokenizer = tokenizer_from_json(data)
    data_comment_clean = list(map(cleanText, data_comment))
    data_comment_tokenizer = tokenizer.texts_to_sequences(data_comment_clean)
    data_comment_tested = pad_sequences(data_comment_tokenizer, maxlen=max_comment_length)
    predictions = model_neiro.predict(data_comment_tested, verbose=1)
    returned_prediction = []
    for pred in predictions:
        returned_prediction.append(pred)
    return returned_prediction
# model=load_model("model_crypto.h5")
# data=['Делитесь своим мнением по данной компании в комментариях: насколько вы готовы инвестировать в подобный бизнес, считаете ли его перспективным? Может ли он попасть в ваш инвестпортфель или нет? Я намеренно не даю название компании, чтобы не влиять на ваше решение - оно будет максимально объективным, если принимается на основании голых цифр. Ну и конечно пишите, насколько вообще интересен и полезен такой формат постов на канале.']
# print(test_my_data(data,model))
