import re
import sqlite3
import pandas as pd
from bs4 import BeautifulSoup
import requests as req
from getWallet import get_wallet
url_start=f"https://bitcointalk.org/index.php?board=83.0"
# получение номера последней страницы
PATH_DB= "collect_crypto_forum.db"
PATH_CSV= "collect_crypto_forum.csv"
def connect_db(path):
    conn = sqlite3.connect(path)
    db = conn.cursor()
    return conn, db

def init_table():
    conn, db = connect_db(PATH_DB)
    db.execute(f"""CREATE TABLE IF NOT EXISTS forum(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        url_discussion TEXT,
        type_address TEXT,
        address TEXT);
        """)

    conn.commit()

def create_forum():
    counter_wallet=0
    counter_link=0
    init_table()
    resp = req.get(url_start)
    soup = BeautifulSoup(resp.text, "html.parser")
    r = soup.find(id="toppages")
    w = r.find_all('a')
    max_page = (int(w[-2].text)) * 40
    # проход по страницам форума
    conn, db = connect_db(PATH_DB)
    for page in range(0, max_page, 40):
        url = f"https://bitcointalk.org/index.php?board=83.{page}"
        resp = req.get(url)
        soup = BeautifulSoup(resp.text, "html.parser").find('div', id='bodyarea')
        all_tr = soup.find(class_="tborder").find_all("tr")
        for tr in all_tr:
            span = tr.find("span")
            if not span:
                continue
            link = span.find("a")["href"]
            text_link = BeautifulSoup(req.get(link).text, "html.parser").get_text(" ", strip=False)
            counter_link+=1
            print(f"link - {counter_link}")
            text_link = re.sub(r"https://[0-9a-zA-Z\.-_/]\S+", ' ', text_link)
            result = get_wallet(text_link)
            if result:
                for adress, type in result.items():
                    counter_wallet+=1
                    print(counter_wallet)
                    sql = "INSERT INTO forum (url_discussion, type_address, address) VALUES (?,?,?)"
                    data = (link, type, adress)
                    db.execute(sql, data)
                    conn.commit()

create_forum()