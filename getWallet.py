import re


def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k


# "":r"",
def get_wallet(text):
    cryptoPatternDict = {"monero": r"4[0-9AB][1-9A-HJ-NP-Za-km-z]{93}",
                         "ethereum": r"0x[0-9a-zA-Z]{40}",
                         "dash": r"X[1-9A-HJ-NP-Za-km-z]{33}",
                         "neo": r"A[0-9a-zA-Z]{33}",
                         "ripple": r"r[0-9a-zA-Z]{33}",  # не захватывает r
                         "dogecoin": r"D{1}[5-9A-HJ-NP-U]{1}[1-9A-HJ-NP-Za-km-z]{32}",
                         "litecoin": r"[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}",
                         "bitcoin": r"[13][a-km-zA-HJ-NP-Z1-9]{25,35}",
                         "bitcoinNew": r"bc1[ac-hj-np-z02-9]{8,87}"
                         }

    walletDict = {}
    for i in cryptoPatternDict.values():
        # walletList = []
        wl = re.findall(i, text)

        for el in wl:
            # print(f"{get_key(cryptoPatternDict, i)} - {el}")
            walletDict.update({el: get_key(cryptoPatternDict, i)})
            text = text.replace(el, ' ')

        walletDict.update({})

    return walletDict

#
# s = '''rMwjYedjc7qqtKYVLiAccJSmCwih4LnE2q - это XRP кошелек
#  ресурсы 3NAM1xaSvmZpgXfTMMJKgxJjHRT5goJXq6, которые 3NAM1xaSvmZpgXfTMMJKgxJjHRT5goJXq6 3NAM1xaSvmZpgXfTMMJKgxJjHRT5goJXq6  0x6b22dbf874cc68080c9832b6fcfa9a8b4f1df487 я применяю при проведении OSINT-исследований, bc1q8z06tmr5x9xalty22432axuveefkxp8486tnhs связанных с анализом криптовалютных транзакций. Интересовать тут меня будет возможность идентификации владельцев цифровых активов и сбора аналитической базы исследования. Подробное '''
# print(get_wallet(s))

