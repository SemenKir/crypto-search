import sys
import getPost as gp
from telethon import TelegramClient
from bot.config import api_id, api_hash

client = TelegramClient('anon', api_id, api_hash)


async def main():
    await gp.dump_all_mess(client, 'irozysk')
    # await gp.get_link(client, 'RBCCrypto')
    sys.exit()


with client:
    client.loop.run_until_complete(main())
    # client.loop.run_until_complete(gp.dump_all_mess(client, 'irozysk'))
    client.start()
    client.run_until_disconnected()

# https://t.me/vklader
# https://t.me/RBCCrypto