import sqlite3
import csv

path = '/Users/asdfg/PycharmProjects/crypto-search/database/test16.db'


def connect_db():
    conn = sqlite3.connect(path)
    db = conn.cursor()
    return conn, db


def init_table(channel):
    conn, db = connect_db()

    db.execute(f"""CREATE TABLE IF NOT EXISTS wallet_{channel}(
        idPost INT PRIMARY KEY,
        urlPost TEXT, 
        channelName TEXT, 
        date DATETIME,  
        mess TEXT,
        wallet TEXT);
        """)

    db.execute(f"""CREATE TABLE IF NOT EXISTS classif_{channel}(
            idPost INT PRIMARY KEY,
            urlPost TEXT, 
            channelName TEXT, 
            date DATETIME,  
            mess TEXT,
            classif TEXT);
            """)

    conn.commit()


def add_post(idPost, urlPost, channelName, date, mess, wallet):
    conn, db = connect_db()
    db.execute(
        f"INSERT INTO wallet_{channelName} (idPost, urlPost, channelName, date, mess, wallet) "
        f"VALUES (?, ?, ?, ?, ?, ?)",
        (idPost, urlPost, channelName, date, mess, wallet))
    conn.commit()
    conn.close()


def add_post_classif(idPost, urlPost, channelName, date, mess, classif):
    conn, db = connect_db()
    db.execute(
        f"INSERT INTO classif_{channelName} (idPost, urlPost, channelName, date, mess, classif) "
        f"VALUES (?, ?, ?, ?, ?, ?)",
        (idPost, urlPost, channelName, date, mess, classif))
    conn.commit()
    conn.close()


def convert_table_and_show_all(tableName):
    """
    перенос данных из sqlite в csv
    решение плохое тк data хранит в себе все данные
    но так как таблица небольшое то решение приелимо
    потом исправить
    :return:
    """
    # os.remove('/Users/asdfg/PycharmProjects/horse-rental/db/data.csv')

    conn, db = connect_db()
    db.execute(f'PRAGMA table_info({tableName})')
    data = db.fetchall()
    headlines = ''
    for i in data:
        headlines += f"{i[1]} "

    db.execute(f"SELECT * FROM {tableName}")
    data = db.fetchall()
    conn.close()

    if data:
        with open('/Users/asdfg/PycharmProjects/crypto-search/database/data.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(headlines.split())
            for row in data:
                writer.writerow(row)
        return True

    else:
        return False


def check_table(tableName):
    conn, db = connect_db()

    try:
        db.execute(f"SELECT * from {tableName} limit 1")
        conn.commit()
        data = db.fetchall()
        conn.close()
        if data:
            return True
        return False

    except sqlite3.OperationalError:
        return False