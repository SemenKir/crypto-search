import re
import emoji
import writeToCSV as wcsv
from database import dbdb
import getWallet as gw

PATH_TO_MODEL = '/Users/asdfg/PycharmProjects/crypto-search/model_crypto.h5'

def init_neuron():
    """
    инициализация нейронной сети
    """
    from tensorflow.keras.models import load_model

    return load_model(PATH_TO_MODEL)


async def dump_all_messages_csv(client, channel):
    """
    функция осуществляет сбор всех постов и запись их
    в csv файл для дальнейшего обучения нейронной сети
    """

    full_url_chanel = f'https://t.me/{channel}/'

    print(f"Начинаю сбор по каналу {channel}")
    async for response in client.iter_messages(channel):
        postId= response.id
        mess = response.message

        if postId == 1:
            print('Сбор постов окончен')
            break

        if mess:
            cleanMess = re.sub(emoji.get_emoji_regexp(), r"", mess)
            wcsv.write_post(cleanMess, '1')
            print(postId)


async def dump_all_mess(client, channel):

    dbdb.init_table(channel)

    urlChannel = f'https://t.me/{channel}/'

    print(f"Начинаю сбор по каналу {channel}")
    async for response in client.iter_messages(channel):
        mess = response.message
        postId = response.id

        if mess:
            mess = re.sub(r"https://[0-9a-zA-Z\.-_/]\S+", ' ', mess)
            walletList = gw.get_wallet(mess)
            if walletList:
                urlPost = urlChannel + str(postId)
                date = response.date
                wallets = ' '.join(walletList.keys())
                dbdb.add_post(postId, urlPost, channel, date, mess, wallets)

        yield postId


async def get_link(client, channel):

    linkList = []
    print(f"Начинаю сбор по каналу {channel}")

    async for response in client.iter_messages(channel):
        mess = response.message
        postId = response.id
        if mess:
            url = re.search(r"https://www.rbc.ru/crypto/.*\w", mess)
            if url:
                linkList.append(url.group())
                print(postId)

    print(linkList)


async def get_info(client, entity):
    """
    получает информацию о канале/чате/пользователе
    """
    e = await client.get_entity(entity)
    return e


async def dump_post_classif(client, channel):
    """

    :return:
    """
    from lerning_ner_network import test_my_data

    print("Подключаю нейронную сеть, ждите...")
    model = init_neuron()
    dbdb.init_table(channel)


    cleanMessList = []
    urlChannel = f'https://t.me/{channel}/'

    print(f"Начинаю сбор по каналу {channel}")
    async for response in client.iter_messages(channel):
        mess = response.message
        postId = response.id

        if mess:
            cleanMess = re.sub(emoji.get_emoji_regexp(), r"", mess)
            print(postId)

            if cleanMess:
                urlPost = urlChannel + str(postId)
                date = response.date
                cleanMessList.append(cleanMess)
                responcePredict = test_my_data(cleanMessList, model)
                print(responcePredict[0], float(responcePredict[0]))
                ton = float(responcePredict[0])

                dbdb.add_post_classif(postId, urlPost, channel, date, mess, ton)

        yield postId


async def quantity_post(client, channel):
    """
    функция получает количество постов на канале
    :param client:
    :param channel:
    :return:
    """
    resp = await client.get_messages(channel)
    for i in resp:
        if i.id:
            return i.id
