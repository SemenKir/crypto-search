#!venv/bin/python
import asyncio
from telethon import TelegramClient, events, utils
from bot.configBot import TOKEN
from telethon import types as tl_types
from telethon.events import StopPropagation
from telethon.sessions import StringSession
from aiogram import Bot, Dispatcher, types
from bot.config import api_id, api_hash
import keyboard as kb
import getPost as gp
import re
from database import dbdb

clients = []

PATH_TO_CSV = '/Users/asdfg/PycharmProjects/crypto-search/database/data.csv'
flagWallet = False
flagClassif = False
userDict = {}


async def run_client(client):
    """
    Запуск Telethon-сессий
    """
    global clients
    clients.append(client)
    async with client:
        # await client.get_dialogs()  # кэшируем список чатов
        await client.start()
        await client.run_until_disconnected()


async def main():
    client = TelegramClient('anon', api_id, api_hash)
    bot = Bot(token=TOKEN)
    dp = Dispatcher(bot)

    @dp.message_handler(commands=['start'])
    async def process_start_command(message: types.Message):
        await message.reply("Здравствуйте!", reply_markup=kb.start_keyboard())

    @dp.callback_query_handler(lambda call: True)
    async def callback_inline(call):
        userId = call.from_user.id

        if call.message:

            if call.data[0:3] == 'yes':
                nameChannel = call.data.split('-')[1]
                if dbdb.convert_table_and_show_all(nameChannel):
                    await bot.send_document(userId, open(PATH_TO_CSV, 'rb'))

            elif call.data == 'no':
                await bot.send_message(userId, 'Ну... ладно.')

    @dp.message_handler()
    async def rep_message(message: types.Message):
        global flagWallet, flagClassif
        userId = message.from_user.id

        mess = message.text.lower()
        messNameChannel = re.fullmatch(r"[0-9a-zA-Z-\/:_.]{5,}", message.text)

        if messNameChannel and flagWallet:
            nameChannel = message.text
            flagWallet = False

            try:
                g = await gp.get_info(client, nameChannel)
                if g.title:

                    if '/' in nameChannel:
                        b = nameChannel.rfind('/')
                        nameChannel = nameChannel[b+1:]

                    if dbdb.check_table(f"wallet_{nameChannel}"):
                        await bot.send_message(userId, "Такая таблица есть, хотите ее получить",
                                               reply_markup=kb.done_keyboard(f"wallet_{nameChannel}"))

                    else:
                        await message.reply(f"Начинаю сбор по каналу {g.title}. Ждите...")
                        quantityPosts = await gp.quantity_post(client, nameChannel)
                        m = await bot.send_message(userId, f'До конца сбора: {quantityPosts} постов')
                        async for postId in gp.dump_all_mess(client, nameChannel):
                            if postId % 100 == 0:
                                await bot.edit_message_text(f'До конца сбора: {postId} постов',
                                                            chat_id=message.chat.id,
                                                            message_id=m['message_id'])
                            if postId == 1:
                                await bot.edit_message_text(f'Сбор окончен!',
                                                            chat_id=message.chat.id,
                                                            message_id=m['message_id'])
                                if dbdb.convert_table_and_show_all(f"wallet_{nameChannel}"):
                                    await bot.send_document(userId, open(PATH_TO_CSV, 'rb'))

            except ValueError:
                await bot.send_message(userId, "Такого канала не существует")

            except AttributeError:
                await bot.send_message(userId, "Похоже Вы отправили не канал")

        elif messNameChannel and flagClassif:
            nameChannel = message.text
            flagClassif = False

            try:
                g = await gp.get_info(client, nameChannel)
                if g.title:

                    if '/' in nameChannel:
                        b = nameChannel.rfind('/')
                        nameChannel = nameChannel[b+1:]

                    if dbdb.check_table(f"classif_{nameChannel}"):
                        await bot.send_message(userId, "Такая таблица есть, хотите ее получить",
                                               reply_markup=kb.done_keyboard(f"classif_{nameChannel}"))

                    else:
                        await message.reply(f"Подключаю нейронную сеть для сбора с канала: {g.title}. Ждите...")
                        quantityPosts = await gp.quantity_post(client, nameChannel)
                        m = await bot.send_message(userId, f'До конца сбора: {quantityPosts} постов.'
                                                           f'Потребуется {int(quantityPosts/60)} мин. {quantityPosts//60} сек.')
                        async for postId in gp.dump_post_classif(client, nameChannel):
                            if postId % 50 == 0:
                                await bot.edit_message_text(f'До конца сбора: {postId} постов.'
                                                            f'Потребуется {int(postId/60)} мин. {postId//60} сек.',
                                                            chat_id=message.chat.id,
                                                            message_id=m['message_id'])
                            if postId == 1:
                                await bot.edit_message_text(f'Сбор окончен!',
                                                            chat_id=message.chat.id,
                                                            message_id=m['message_id'])
                                if dbdb.convert_table_and_show_all(f"classif_{nameChannel}"):
                                    await bot.send_document(userId, open(PATH_TO_CSV, 'rb'))

            except ValueError:
                await bot.send_message(userId, "Такого канала не существует")

            except AttributeError:
                await bot.send_message(userId, "Похоже Вы отправили не канал")

        elif mess == 'найти кошельки':
            flagWallet = True
            await bot.send_message(userId, "Отправте название канала")

        elif mess == 'анализ канала':
            flagClassif = True
            await bot.send_message(userId, "Отправте название канала")

        else:
            await message.reply("Я Вас не понимаю")

    sessions = []

    sessions.append(run_client(client))

    # Пихаем поллинг бота в массив
    sessions.append(dp.start_polling())

    # Запускаем
    await asyncio.gather(*sessions)


# Точка входа в программу
if __name__ == '__main__':
    asyncio.run(main())

