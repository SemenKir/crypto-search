from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton


def start_keyboard():
    buttonSearchWallet = KeyboardButton('Найти кошельки')
    buttonAnalys = KeyboardButton('Анализ канала')

    buttonChoce = ReplyKeyboardMarkup()

    return buttonChoce.add(buttonSearchWallet, buttonAnalys)


def done_keyboard(channelName):
    yes = InlineKeyboardButton('Да', callback_data=f'yes-{channelName}')
    no = InlineKeyboardButton('Нет', callback_data='no')

    doneKeyboard = InlineKeyboardMarkup().add(yes, no)

    return doneKeyboard

